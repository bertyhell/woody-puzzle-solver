from ludwig.api import LudwigModel
import json
import os

class LudwigProxy:
    models = {}

    # @staticmethod
    # def train(training_samples, model_definitions, model_name):
    #     LudwigProxy.models[model_name] = LudwigModel(model_definitions)
    #     train_stats = LudwigProxy[model_name].train(training_samples)
    #     return {'message': 'train: ok'}

    @staticmethod
    def predict(model_name, data_df):
        # check if model is loaded
        if not model_name in LudwigProxy.models:
            # check model files exists
            model_dir = ".\\models\\" + model_name + "\\model"
            if os.path.isdir(model_dir):
                LudwigProxy.models[model_name] = LudwigModel.load(model_dir)
            else:
                return {'message': 'The model with name ' + model_name + ' was not found. Directory doesn\'t exist: ' + model_dir}

        predictions = LudwigProxy.models[model_name].predict(data_df=data_df)
        return {'message': 'ok', 'results': json.loads(predictions.to_json())}
