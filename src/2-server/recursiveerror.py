class RecursiveError:

    def __init__(self, error_message, err=None, additional_info=None):
        self.error_message = error_message
        self.err = err
        self.additional_info = additional_info

    def to_json(self):
        return {
            'errorMessage': self.error_message,
            'err': self.err,
            'additionalInfo': self.additional_info,
        }
