export const options = {
	generation: 0, // '1-trained-on-1000';
	numberOfMatches: 2000000,
	batchSize: 1000,
	threads: 11,
};
